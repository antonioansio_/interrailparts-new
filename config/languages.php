<?php

return [
    'en' => [
        'display' => 'English (EN)',
        'flag-icon' => 'us'
    ],
    'es' => [
        'display' => 'Spanish (ES)',
        'flag-icon' => 'es'
    ],
    'fr' => [
        'display' => 'French (FR)',
        'flag-icon' => 'fr'
    ]
];
