<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Interrailparts</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<header>
    <div class="container">
        <div class="d-flex align-items-center justify-content-between">
            <div class="logo">
                <img src="https://interrailparts.s3.eu-central-1.amazonaws.com/logo-new.jpg" alt="Interrailparts">
            </div>
            <div class="returnBack">
                <a href="{{ url('/') }}">{{ __('offer.return') }}</a>
            </div>
        </div>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="py-5">
                <h1 style="margin-bottom: 20px;">{{ __('offer.title') }}</h1>
                <p>{{ __('offer.se-aplica') }}</p>
                <p>{{ __('offer.vendemos') }}</p>
                <p>{{ __('offer.tipos') }}</p>
                <p>{{ __('offer.establecer') }}</p>
                <p>{{ __('offer.conjunto') }}</p>
                <p>{{ __('offer.nuestra') }}</p>
                <p>{{ __('offer.vendemos-juegos') }}</p>
                <p>{{ __('offer.colaboramos') }}</p>
                <p>{{ __('offer.adjuntamos') }}</p>
                <p>{{ __('offer.kits') }}</p>
                <p>{{ __('offer.invitamos') }}</p>
                <p>{!!  __('offer.pawel') !!}</p>
            </div>
        </div>
    </div>
</div>

<footer class="text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span>© 2021 <span class="fw-bold text-uppercase">Interrailparts</span> - All rights reserved</span>
            </div>
        </div>
    </div>
</footer>

<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
