<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Interrailparts</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <header>
            <div class="container">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="logo">
                        <img src="https://interrailparts.s3.eu-central-1.amazonaws.com/logo-new.jpg" alt="Interrailparts">
                    </div>
                    <div class="offer">
                        @if(app()->getLocale() !== 'fr')
                        <a href="{{ url('/offer') }}" class="btn btn-danger">{{ __('offer.title') }}</a>
                            @endif
                    </div>
                    <div class="language">
                        @foreach (Config::get('languages') as $lang => $language)
                            @if ($lang != App::getLocale())
                                <a href="{{ route('lang.switch', $lang) }}" style="padding-right: 5px;"><span class="flag-icon flag-icon-{{$language['flag-icon']}}"></span> {{$language['display']}}</a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </header>

        <div class="hero">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="mb-3">{{ __('home.welcome') }} <span class="fw-bold">Interrailparts</span></h1>
                        <p>{{ __('home.description') }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="about">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-xl-8 m-auto">
                        <div class="text-center">
                            <h3 class="fw-bold mb-3">{{ __('home.about.title') }}</h3>
                            <p>{{ __('home.about.description') }}</p>
                        </div>
                        <div class="col-md-12">
                            <div class="mt-5">
                                <img class="img-fluid" src="https://interrailparts.s3.eu-central-1.amazonaws.com/pympxydyjguzdhgwxveoombdfleuvksf.jpeg" alt="">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="clients">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-xl-8 m-auto">
                        <div class="text-center">
                            <h3 class="fw-bold mb-3">{{ __('home.clients.title') }}</h3>
                            <p>{{ __('home.clients.description') }} <strong class="d-block mt-3 text-uppercase">{{ __('home.clients.offer') }}</strong></p>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mt-4 mt-lg-4">
                                        <img class="img-fluid" src="https://interrailparts.s3.eu-central-1.amazonaws.com/0fAAmZHosOFq32kj.jpeg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mt-4 mt-lg-4">
                                        <img class="img-fluid" src="https://interrailparts.s3.eu-central-1.amazonaws.com/MVoZEfIPaq5ZM31d.jpeg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mt-4 mt-lg-4">
                                        <img class="img-fluid" src="https://interrailparts.s3.eu-central-1.amazonaws.com/WkquV6Dlwe1mo6L4.jpeg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mt-4 mt-lg-4">
                                        <img class="img-fluid" src="https://interrailparts.s3.eu-central-1.amazonaws.com/eEpqDaUFfhpWlili.jpeg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mt-4 mt-lg-4">
                                        <img class="img-fluid" src="https://interrailparts.s3.eu-central-1.amazonaws.com/ka6fG4k3toWsCpxt.jpeg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mt-4 mt-lg-4">
                                        <img class="img-fluid" src="https://interrailparts.s3.eu-central-1.amazonaws.com/xlqdzy4wqHzOJ3qs.jpeg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="contact">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-xl-8 m-auto">
                        <div class="text-center">
                            <h3 class="fw-bold mb-3">{{ __('home.contact.title') }}</h3>
                            <p>{{ __('home.contact.description') }}</p>
                        </div>
                        <div class="row">
                            <div class="col-md-11 mx-auto">
                                <div class="links">
                                    <a href="mailto:Interrailparts@gmail.com" class="box">
                                        <div><i class="fas fa-envelope"></i></div>
                                        <div class="py-4 pr-5 flex-grow-1">{{ __('home.contact.mail') }}</div>
                                    </a>
                                    <a href="tel:+34655914222" class="box">
                                        <div><i class="fas fa-phone-alt"></i></div>
                                        <div class="py-4 pr-5 flex-grow-1">{{ __('home.contact.phone') }}</div>
                                    </a>
                                    <a href="https://api.whatsapp.com/send?phone=655914222" class="box">
                                        <div><i class="fab fa-whatsapp"></i></div>
                                        <div class="py-4 pr-5 flex-grow-1">{{ __('home.contact.whatsapp') }}</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <span>© 2021 <span class="fw-bold text-uppercase">Interrailparts</span> - All rights reserved</span>
                    </div>
                </div>
            </div>
        </footer>

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
