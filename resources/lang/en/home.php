<?php

return [

    'welcome' => 'Welcome to',
    'description' => 'Our mission as a company is to buy / sell cargo wagon wheelsets. Delivery to the customer, review, quality assurance.',

    'about' => [
        'title' => 'About us',
        'description' => 'Our company founded in 2018 in Barcelona, a department dedicated to searching for the wheelsets throughout Europe and delivering them to all eastern countries, even Morocco Africa, Bulgaria, Germany, etc...'
    ],

    'clients' => [
        'title' => 'Potential customers',
        'description' => 'The company has such potential clients as DB SCHENKER, LOTOS PETROLIFERAS, PKP INTERCITY, DEUTSCHE BAHN giving us the confidence from the beginning of the company in all its services. Buy and sell wagon parts, axles, springs and more!',
        'offer' => 'We offer Y25CS wheels, from 900mm to 920mm'
    ],

    'contact' => [
        'title' => 'Do you want to contact us?',
        'description' => 'We are here for everything you need',
        'mail' => 'Send us a message and receive an answer in your e-mail',
        'phone' => 'If your query is urgent, call us',
        'whatsapp' => 'Send us a WhatsApp and receive answers on your mobile'
    ],

];
