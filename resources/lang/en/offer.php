<?php

return [

    'return' => 'Go back',
    'title' => 'OFFER',
    'se-aplica' => 'Applies to: ø920mm wheel sets',
    'vendemos' => 'We present you an offer for the sale of wheel sets for freight wagons of various types.',
    'tipos' => 'We sell the sets after the IS2 revision repair or for repair.',
    'establecer' => 'Wagon types: Rail tank cars, EAOS, TADS, SGNS, RGS, RES, FALS ...',
    'conjunto' => 'Set diameters from ø890mm to ø920mm',
    'nuestra' => 'Set type: Y25, UIC.',
    'vendemos-juegos' => 'Our company has been operating on the Polish and European market for 25 years.',
    'colaboramos' => 'We sell over 5000 sets per year. We have two repair workshops in Poland, Krotoszyn and Jaworzno.',
    'adjuntamos' => 'We cooperate with DB-Cargo, PKP-Cargo, CTL Logistic, CD Cargo, Mittal, Acelor Katowice (KOLPREM).',
    'kits' => 'We enclose full repair documentation to the sets after the IS2 revision. Certificate 3.1',
    'invitamos' => 'The kits of wheelsets can be viewed at our workshop in Krotoszyn - Poland.',
    'pawel' => 'We invite you to cooperation, <br> Pawel Hryciek Wheelsets Menagment tel: +34655914222',
];
