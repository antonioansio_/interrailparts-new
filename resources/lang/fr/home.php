<?php

return [

    'welcome' => 'Bienvenue à',
    'description' => 'Notre mission en tant qu\'entreprise est d\'acheter / vendre des roues de wagons de marchandises. Livraison au client, examen, assurance qualité.',

    'about' => [
        'title' => 'À propos de nous',
        'description' => 'Notre société fondée en 2018 à Barcelone, un département dédié à la recherche de roues dans toute l\'Europe et à leur livraison dans tous les pays de l\'Est, dont le Maroc, l\'Afrique, la Bulgarie, l\'Allemagne, etc...'
    ],

    'clients' => [
        'title' => 'Clients potentiels',
        'description' => 'L\'entreprise a des clients potentiels tels que DB SCHENKER, LOTOS PETROLIFERAS, PKP INTERCITY, DEUTSCHE BAHN nous donnant la confiance dès le début de l\'entreprise dans tous ses services. Achat et vente de pièces détachées pour wagons, essieux, ressorts et plus!',
        'offer' => 'Nous proposons des roues Y25CS, de 900 mm à 920 mm'
    ],

    'contact' => [
        'title' => 'Voulez-vous nous contacter?',
        'description' => 'Nous sommes là pour tout ce dont vous avez besoin',
        'mail' => 'Envoyez-nous un message et recevez une réponse dans votre e-mail',
        'phone' => 'Si votre demande est urgente, appelez-nous',
        'whatsapp' => 'Envoyez-nous un WhatsApp et recevez des réponses sur votre mobile'
    ],
];
