<?php

return [

    'return' => 'Volver atrás',
    'title' => 'OFERTA',
    'se-aplica' => 'Se aplica a: juegos de ruedas de ø920mm',
    'vendemos' => 'Vendemos los juegos después de la reparación de revisión IS2 o para reparación.',
    'tipos' => 'Tipos de vagones: Vagones cisterna, EAOS, TADS, SGNS, RGS, RES, FALS ...',
    'establecer' => 'Establecer diámetros de ø890mm a ø920mm',
    'conjunto' => 'Tipo de conjunto: Y25, UIC.',
    'nuestra' => 'Nuestra empresa opera en el mercado polaco y europeo desde hace 25 años.',
    'vendemos-juegos' => 'Vendemos 5000 juegos por año. Tenemos dos telleres de reparación en Polonia, Krotoszyn y Jaworzno.',
    'colaboramos' => 'Colaboramos con DB-Cargo, PKP-Cargo, CTL Logistic, CD Cargo, Mittal, Acelor Katowice (KOLPREM).',
    'adjuntamos' => 'Adjuntamos la documentación de reparación completa a los equipos después de la revisión IS2. Certificado 3.1',
    'kits' => 'Los kits se pueden ver en nuestro taller de Krotoszyn - Polonia.',
    'invitamos' => 'Te invitamos a la cooperación,',
    'pawel' => 'Pawel Hryciek Juego de ruedas Menagment tel: +34655914222',
];
