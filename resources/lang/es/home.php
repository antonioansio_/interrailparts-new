<?php

return [

    'welcome' => 'Bienvenido a',
    'description' => 'Nuestra misión como empresa es comprar / vender ruedas de vagones de carga. Entrega al cliente, revisión, aseguramiento de la calidad.',

    'about' => [
        'title' => 'Sobre nosotros',
        'description' => 'Nuestra empresa fundada en 2018 en Barcelona, departamento dedicado a buscar ruedas por toda Europa y entregarlos a todos los países del este, incluso Marruecos África, Bulgaria, Alemania, etc...'
    ],

    'clients' => [
        'title' => 'Clientes potenciales',
        'description' => 'La empresa cuenta con clientes potenciales como DB SCHENKER, LOTOS PETROLIFERAS, PKP INTERCITY, DEUTSCHE BAHN dándonos la confianza desde el inicio de la empresa en todos sus servicios. Compra y venta de repuestos de vagones, ejes, muelles y más!',
        'offer' => 'Ofrecemos ruedas Y25CS, de 900mm a 920mm'
    ],

    'contact' => [
        'title' => '¿Quieres contactar con nosotros?',
        'description' => 'Estamos aquí para todo lo que necesites',
        'mail' => 'Envíanos un mensaje y recibe una respuesta en tu e-mail',
        'phone' => 'Si tu consulta es urgente llámanos',
        'whatsapp' => 'Envíanos un WhatsApp y recibe respuestas en tu móvil'
    ],
];
